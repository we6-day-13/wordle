import requests as rq
import random


class MMBot:
    words = [word.strip() for word in open("words.txt")]
    mm_url = "https://we6.talentsprint.com/wordle/game/"
    register_url = mm_url + "register"
    creat_url = mm_url + "create"
    guess_url = mm_url + "guess"
    SINGLETON = True
    secret = "RRRRR"
    def __init__(self, name: str):
        if MMBot.SINGLETON:
            MMBot.SINGLETON = False
            self.session = rq.session()
        register_dict = {"mode": "wordle", "name": name}
        reg_resp = self.session.post(MMBot.register_url, json=register_dict)
        self.me = reg_resp.json()['id']

    def setup_game(self):
        self.attempts = 0
        creat_dict = {"id": self.me, "overwrite": True}
        self.session.post(MMBot.creat_url, json=creat_dict)
        self.choices = [w for w in MMBot.words]
        random.shuffle(self.choices)

    def play(self) -> dict:
        def post(choice: str) -> tuple[str, bool]:
            guess = {"id": self.me, "guess": choice}
            response = self.session.post(MMBot.guess_url, json=guess)
            rj = response.json()
            matches = rj["feedback"]
            status = "win" in rj["message"]
            return matches, status

        choice = random.choice(self.choices)
        self.choices.remove(choice)
        matches, won = post(choice)
        tries = [f'{choice}:{matches}']

        while not won:
<<<<<<< HEAD
			  if DEBUG:
				  print(choice, matches, self.choices[:10])
			  self.update(choice, matches)
           choice = random.choice(self.choices)
           self.choices.remove(choice)
           matches, won = post(choice)
           tries.append(f'{choice}:{matches}')
=======

            if DEBUG:
                print(choice, matches, self.choices[:10])

            self.update(choice, matches)
            choice = random.choice(self.choices)
            self.choices.remove(choice)
            matches, won = post(choice)
            tries.append(f'{choice}:{matches}')
>>>>>>> 2ba07eb80324f083f8be8bad0e2b4948d8054548
        return {"Secret": choice,
                "Attempts": len(tries),
                "Route": " => ".join(tries)}

    def update(self, choice: str, matches: str):
       def common(choice: str, word: str):
            return len(set(choice) & set(word))

       if matches == "RRRRR":
            self.choices = [w for w in self.choices if common(choice, w) == 0]
		 
		 if 'Y' in matches:
			 match_found = matches.count('Y')
			 self.choices = [w for w in self.choices if common(choice, w) <= match_found]
       
		 if 'G' in matches:
			 desired = []
			 TEST = 1
			 pos = [matches.index(i) for i in matches if i == 'G']
			 for w in self.choices:
				 for i in pos:
					 if w[i] != choice[i]:
						 TEST = 0
				 if TEST:
					 desired.append(w)
			 self.choices = desired	 	 

       if 'G' in matches:
            pos = matches.index('G')
            self.secret.replace(self.secret[pos],choice[pos])
			   self.choices = [w for w in self.choices if common(choice, w) == match_found]

			 


DEBUG = False
bot = MMBot("hello")
for _ in range(10):
    bot.setup_game()
    print(bot.play())
